from pdf2image import convert_from_path
from progress.bar import Bar
import glob

arr = glob.glob("pdf2/*")

def convertPdf(item):
    # contador = 0
    total = len(item)
    bar = Bar('Processing', max=total)
    for key in item:
        bar.next()
        # contador += 1
        # print(contador, total)
        try:
            pages = convert_from_path(key, 500)
            for page in pages:
                page.save(key +'.jpg', 'JPEG')
        except Exception as e:
            print('err')
    bar.finish()

convertPdf(arr)

# pages = convert_from_path('pdf/100012754', 500)
#
# for page in pages:
#     page.save('out.jpg', 'JPEG')
