var fs = require('fs');
var Promise = require('bluebird')
var file = fs.readFileSync('log_no.txt', 'utf8')
var excelToJson = require('convert-excel-to-json');
var ctrl = require('./controllers/pad');

file = file.split('\t')

var uniq = [ ...new Set(file) ];
console.log(uniq.length,'<---------------');

const result = excelToJson({
    sourceFile: 'lay.xlsx',
    columnToKey: {
        A: 'FECHA_ELABORACION',
        B: 'DESPACHO',
        C: 'NUMERO_DE_CREDITO',
        D: 'TIPO_DE_CREDITO',
        E: 'POOL',
        F: 'NOMBRE_DEL_ACREDITADO',
        G: 'ESTATUS',
        H: 'OMISOS',
        I: 'TIPO_DE_CREDITO',
        J: 'VSM',
        K: 'ADEUDO_TOTAL',
        L: 'PESOS',
        M: 'Calle_No',
        N: 'Colonia',
        O: 'Delegacion',
        P: 'Poblacion',
        Q: 'CP',
        R: 'Referencias',
        S: 'DESHABITADA',
        T: 'ABANDONADA',
        U: 'VANDALIZADA',
        V: 'TERRENO_BALDIO',
        W: 'DANOS_ESTRUCTURALES',
        X: 'COMODATO',
        Y: 'USO_COMERCIAL ',
        Z: 'TRASPASO',
        AA: 'Entre_calle',
        AB: 'Y',
        AC: 'Observaciones',
        AD: 'Elaboro',
        AE: 'Autorizo'
    }
});

var arr1 = result.LAYOUT
arr1 = arr1.slice(1)

var arr_ok = []

for (var key in arr1) {
  for (var key1 in uniq) {
    if (arr1[key].NUMERO_DE_CREDITO*1 === uniq[key1]*1) {
      arr_ok.push(arr1[key])
    }
  }
}

console.log(arr_ok.length);

var contador = 0

Promise.each(arr_ok, function(item){
  return new Promise(function(resolve, reject) {
    contador++
    item.Referencias = item.Referencias === undefined ? ' ' : item.Referencias
    item.DESHABITADA = item.DESHABITADA === undefined ? ' ' : item.DESHABITADA
    item.ABANDONADA = item.ABANDONADA === undefined ? ' ' : item.ABANDONADA
    item.VANDALIZADA = item.VANDALIZADA === undefined ? ' ' : item.VANDALIZADA
    item.TERRENO_BALDIO = item.TERRENO_BALDIO === undefined ? ' ' : item.TERRENO_BALDIO
    item.DANOS_ESTRUCTURALES = item.DANOS_ESTRUCTURALES === undefined ? ' ' : item.DANOS_ESTRUCTURALES
    item.COMODATO = item.COMODATO === undefined ? ' ' : item.COMODATO
    item.USO_COMERCIAL = item.USO_COMERCIAL === undefined ? ' ' : item.USO_COMERCIAL
    item.TRASPASO = item.TRASPASO === undefined ? ' ' : item.TRASPASO
    // console.log(item);
    console.log(`${contador} de ${arr_ok.length}`);
    try {
      // console.log('entro');
      var v = fs.readFileSync(__dirname + '/controllers/fotos_pdf2/' + item.NUMERO_DE_CREDITO + '.pdf.jpg')
      // console.log(item.NUMERO_DE_CREDITO,'<-----------CREDITO ENCONTRADO');
      ctrl.crearWord(item)
      .then(function(res){
        fs.appendFile('log_si.txt', item.NUMERO_DE_CREDITO + '\t', function (err) {
          if (err) throw err;
          resolve()
        });
      })
    } catch (e) {
      console.log('entro');
      fs.appendFile('log_no.txt', item.NUMERO_DE_CREDITO + '\t', function (err) {
        if (err) throw err;
        resolve()
      });
    }
  });
}).then(function(res){
  console.log('termino');
})
