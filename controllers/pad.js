var ImageModule=require('docxtemplater-image-module')
var JSZip = require('jszip');
var Docxtemplater = require('docxtemplater');
var fs = require('fs');
var path = require('path');

module.exports.crearWord = function(d) {
  return new Promise(function(resolve, reject) {
    var content = fs.readFileSync(path.resolve(__dirname, 'hoja1.docx'), 'binary');
    var opts = {}
    opts.centered = false;
    opts.getImage=function(tagValue, tagName) {
        return fs.readFileSync(tagValue);
    }

    opts.getSize=function(img,tagValue, tagName) {
        return [250,250];
    }

    var imageModule=new ImageModule(opts);

    var zip = new JSZip(content);
    var docx=new Docxtemplater()
    docx.attachModule(imageModule)
    docx.loadZip(zip)
    var mi_ruta = ""
    if (d.where === 1) {
      mi_ruta = __dirname + `/fotos/${d.NUMERO_DE_CREDITO}.jpeg`
    } else if (d.where === 2) {
      mi_ruta = __dirname + `/fotos/${d.NUMERO_DE_CREDITO}.pdf.jpg`
    } else if (d.where === 3) {
      mi_ruta = __dirname + `/fotos_pdf2/${d.NUMERO_DE_CREDITO}.pdf.jpg`
    }

    docx.setData({
         FECHA_ELABORACION: d.FECHA_ELABORACION,
         DESPACHO: d.DESPACHO,
         NUMERO_DE_CREDITO: d.NUMERO_DE_CREDITO,
         TIPO_DE_CREDITO: d.TIPO_DE_CREDITO,
         POOL: d.POOL,
         NOMBRE_DEL_ACREDITADO: d.NOMBRE_DEL_ACREDITADO,
         ESTATUS: d.ESTATUS,
         OMISOS: d.OMISOS,
         TIPO_DE_CREDITO: d.TIPO_DE_CREDITO,
         VSM: d.VSM,
         ADEUDO_TOTAL: d.ADEUDO_TOTAL,
         PESOS: d.PESOS,
         Calle_No: d.Calle_No,
         Colonia: d.Colonia,
         Delegacion: d.Delegacion,
         Poblacion: d.Poblacion,
         CP: d.CP,
         de: d.DESHABITADA,
         ab: d.ABANDONADA,
         va: d.VANDALIZADA,
         te: d.TERRENO_BALDIO,
         da: d.DANOS_ESTRUCTURALES,
         co: d.COMODATO,
         us: d.USO_COMERCIAL,
         tr: d.TRASPASO,
         en: d.Entre_calle,
         yc: d.Y,
         en1: d.Entre_calle,
         yc1: d.Y,
         Referencias: d.Referencias,
         Observaciones: d.Observaciones,
         Elaboro: d.Elaboro,
         Autorizo: d.Autorizo,
         image: mi_ruta,
         geo: geo
    });

    try {
        // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
        docx.render()
    }
    catch (error) {
        var e = {
            message: error.message,
            name: error.name,
            stack: error.stack,
            properties: error.properties,
        }
        console.log(JSON.stringify({error: e}));
        fs.appendFile('log_err.txt', d.NUMERO_DE_CREDITO + '\t', function (err) {
          if (err) throw err;
          resolve()
        });
        // The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
        // throw error;
    }

    var buffer= docx.getZip().generate({type:"nodebuffer", compression: "DEFLATE"});

    fs.writeFileSync(__dirname + `/word_final/${d.NUMERO_DE_CREDITO}.docx`, buffer);
    resolve({ err: false })
  });
}
