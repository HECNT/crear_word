const express = require('express');
const app = express();
const PORT = 3001;

app.use(express.static(__dirname + '/controllers'));

app.get('/', init);

function init(req, res) {
  res.send('Activo')
}

app.listen(PORT, function () {
  console.log(`Escuchando en el puerto ${PORT}`);
})
