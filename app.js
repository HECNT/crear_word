var excelToJson = require('convert-excel-to-json');
var ctrl = require('./controllers/pad');

var Promise = require('bluebird')
var fs = require('fs')

const result = excelToJson({
    sourceFile: 'lay.xlsx',
    columnToKey: {
        A: 'FECHA_ELABORACION',
        B: 'DESPACHO',
        C: 'NUMERO_DE_CREDITO',
        D: 'TIPO_DE_CREDITO',
        E: 'POOL',
        F: 'NOMBRE_DEL_ACREDITADO',
        G: 'ESTATUS',
        H: 'OMISOS',
        I: 'TIPO_DE_CREDITO',
        J: 'VSM',
        K: 'ADEUDO_TOTAL',
        L: 'PESOS',
        M: 'Calle_No',
        N: 'Colonia',
        O: 'Delegacion',
        P: 'Poblacion',
        Q: 'CP',
        R: 'Referencias',
        S: 'DESHABITADA',
        T: 'ABANDONADA',
        U: 'VANDALIZADA',
        V: 'TERRENO_BALDIO',
        W: 'DANOS_ESTRUCTURALES',
        X: 'COMODATO',
        Y: 'USO_COMERCIAL ',
        Z: 'TRASPASO',
        AA: 'Entre_calle',
        AB: 'Y',
        AC: 'Observaciones',
        AD: 'Elaboro',
        AE: 'Autorizo',
        AF: 'lat',
        AG: 'lon'
    }
});

var arr1 = result.LAYOUT
arr1 = arr1.slice(1)
var contador = 0

Promise.each(arr1, function(item){
  return new Promise(function(resolve, reject) {
    contador++
    item.geo = item.lat + ',' + item.lon
    item.Referencias = item.Referencias === undefined ? ' ' : item.Referencias
    item.DESHABITADA = item.DESHABITADA === undefined ? ' ' : item.DESHABITADA
    item.ABANDONADA = item.ABANDONADA === undefined ? ' ' : item.ABANDONADA
    item.VANDALIZADA = item.VANDALIZADA === undefined ? ' ' : item.VANDALIZADA
    item.TERRENO_BALDIO = item.TERRENO_BALDIO === undefined ? ' ' : item.TERRENO_BALDIO
    item.DANOS_ESTRUCTURALES = item.DANOS_ESTRUCTURALES === undefined ? ' ' : item.DANOS_ESTRUCTURALES
    item.COMODATO = item.COMODATO === undefined ? ' ' : item.COMODATO
    item.USO_COMERCIAL = item.USO_COMERCIAL === undefined ? ' ' : item.USO_COMERCIAL
    item.TRASPASO = item.TRASPASO === undefined ? ' ' : item.TRASPASO
    item.Observaciones = item.Observaciones === undefined ? ' ' : item.Observaciones
    // console.log(item);
    console.log(`${contador} de ${arr1.length}`);
    try {
      var v = fs.readFileSync(__dirname + '/controllers/fotos/' + item.NUMERO_DE_CREDITO + '.jpeg')
      // console.log(item.NUMERO_DE_CREDITO,'<-----------CREDITO ENCONTRADO');
      item.where = 1
      ctrl.crearWord(item)
      .then(function(res){
        fs.appendFile('log_si.txt', item.NUMERO_DE_CREDITO + '\t', function (err) {
          if (err) throw err;
          resolve()
        });
      })
    } catch (e) {
      try {
        var v = fs.readFileSync(__dirname + '/controllers/fotos/' + item.NUMERO_DE_CREDITO + '.pdf.jpg')
        // console.log(item.NUMERO_DE_CREDITO,'<-----------CREDITO ENCONTRADO');
        item.where = 2
        ctrl.crearWord(item)
        .then(function(res){
          fs.appendFile('log_si.txt', item.NUMERO_DE_CREDITO + '\t', function (err) {
            if (err) throw err;
            resolve()
          });
        })
      } catch (e) {
        // try {
        //   var v = fs.readFileSync(__dirname + '/controllers/fotos_pdf2/' + item.NUMERO_DE_CREDITO + '.pdf.jpg')
        //   // console.log(item.NUMERO_DE_CREDITO,'<-----------CREDITO ENCONTRADO');
        //   item.where = 3
        //   ctrl.crearWord(item)
        //   .then(function(res){
        //     fs.appendFile('log_si.txt', item.NUMERO_DE_CREDITO + '\t', function (err) {
        //       if (err) throw err;
        //       resolve()
        //     });
        //   })
        // } catch (e) {
        // }
        console.log('no<--------------------');
        fs.appendFile('log_no.txt', item.NUMERO_DE_CREDITO + '\t', function (err) {
          if (err) throw err;
          resolve()
        });
      }
    }
  });
}).then(function(res){
  console.log('termino');
})


// var JSZip = require('jszip');
// var Docxtemplater = require('docxtemplater');
//
// var fs = require('fs');
// var path = require('path');
//
// //Load the docx file as a binary
// var content = fs.readFileSync(path.resolve(__dirname, 'hoja.docx'), 'binary');
//
// var zip = new JSZip(content);
//
// var doc = new Docxtemplater();
// doc.loadZip(zip);
//
// //set the templateVariables
// doc.setData({
//      fecha_elaboracion: "dsada"
// });
//
// try {
//     // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
//     doc.render()
// }
// catch (error) {
//     var e = {
//         message: error.message,
//         name: error.name,
//         stack: error.stack,
//         properties: error.properties,
//     }
//     console.log(JSON.stringify({error: e}));
//     // The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
//     throw error;
// }
//
// var buf = doc.getZip()
//              .generate({type: 'nodebuffer'});
//
// // buf is a nodejs buffer, you can either write it to a file or do anything else with it.
// fs.writeFileSync(path.resolve(__dirname, 'output.docx'), buf);
